﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator {

    public static float[, ] GenerateFalloffMap(int size) {
        float[, ] map = new float[size, size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                float x = i / (float) size * 2 - 1;
                float y = j / (float) size * 2 - 1;

                float value = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));
                map[i, j] = Smoothstep(value);
            }
        }

        return map;
    }

    static float Smoothstep(float value) {
        float a = 0.5f;
        float b = 1f;

        float k = Mathf.Clamp01((value - a) / (b - a));
        return k * k * (3 - 2 * k);
    }

}